// Copyright 2019 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

package main

type Font struct {
	digits [12]*FontChar
}

type FontChar struct {
	lines []string
}

var font Font

func InitFont() {
	for i := 0; i < 12; i++ {
		font.digits[i] = BasicDigit(i)
	}
}

func BasicDigit(i int) *FontChar {
	fc := new(FontChar)
	fc.lines = make([]string, 0)

	switch i {
	case 0:
		fc.AddLine("####")
		fc.AddLine("#  #")
		fc.AddLine("#  #")
		fc.AddLine("#  #")
		fc.AddLine("####")

	case 1:
		fc.AddLine("  # ")
		fc.AddLine("  # ")
		fc.AddLine("  # ")
		fc.AddLine("  # ")
		fc.AddLine("  # ")

	case 2:
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("####")
		fc.AddLine("#   ")
		fc.AddLine("####")

	case 3:
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("####")

	case 4:
		fc.AddLine("#  #")
		fc.AddLine("#  #")
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("   #")

	case 5:
		fc.AddLine("####")
		fc.AddLine("#   ")
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("####")

	case 6:
		fc.AddLine("####")
		fc.AddLine("#   ")
		fc.AddLine("####")
		fc.AddLine("#  #")
		fc.AddLine("####")

	case 7:
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("   #")
		fc.AddLine("   #")
		fc.AddLine("   #")

	case 8:
		fc.AddLine("####")
		fc.AddLine("#  #")
		fc.AddLine("####")
		fc.AddLine("#  #")
		fc.AddLine("####")

	case 9:
		fc.AddLine("####")
		fc.AddLine("#  #")
		fc.AddLine("####")
		fc.AddLine("   #")
		fc.AddLine("####")

	case 10:
		fc.AddLine("    ")
		fc.AddLine("    ")
		fc.AddLine("    ")
		fc.AddLine("    ")
		fc.AddLine("    ")

	case 11:
		fc.AddLine("   ")
		fc.AddLine(" # ")
		fc.AddLine("   ")
		fc.AddLine(" # ")
		fc.AddLine("   ")
	}

	return fc
}

func (font *Font) GetChar(r rune) *FontChar {
	if '0' <= r && r <= '9' {
		return font.digits[r - '0']
	}
	if r == ' ' {
		return font.digits[10]
	}
	if r == ':' {
		return font.digits[11]
	}
	return nil
}

func (fc *FontChar) AddLine(s string) {
	s2 := ""

	for _, r := range s {
		if r == '#' {
			s2 = s2 + "\x1b[46m \x1b[0m"
		} else {
			s2 = s2 + string(r)
		}
	}

	fc.lines = append(fc.lines, s2)
}

func (fc *FontChar) GetLine(y int) string {
	if y < 0 || y >= len(fc.lines) {
		y = 0
	}
	return fc.lines[y]
}
