// Copyright 2019 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

bigtime : display the current time using big numbers.

by Andrew Apted, 2019.

*/
package main

import "fmt"
import "time"
// import "os"

func RenderLine(y int, time_str string) string {
	if y < 0 || y >= 5 {
		return ""
	}

	s := ""

	for _, r := range time_str {
		ch := font.GetChar(r)

		if ch == nil {
			s = s + "???"
		} else {
			s = s + ch.GetLine(y)
		}

		s = s + " "
	}

	return s
}

func main() {
	InitFont()

	now := time.Now()

	hour, min, _ := now.Clock()

	pm := false
	if hour >= 12 {
		hour = hour % 12
		pm = true
	}

	if hour == 0 {
		hour = 12
	}
	_ = pm

	time_str := fmt.Sprintf("%2d:%02d", hour, min)

	fmt.Printf("\n")

	for y := 0; y < 5; y++ {
		s := RenderLine(y, time_str)

		if y == 4 {
			if pm {
				s = s + " P M "
			} else {
				s = s + " A M "
			}
		}

		fmt.Printf("  %s\n", s)
	}

	fmt.Printf("\n")
}
